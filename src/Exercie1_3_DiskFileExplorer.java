import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class Exercie1_3_DiskFileExplorer {

    //La methode recupere le path en entrée et retourne  :
    // Si c'est un dossierla liste des fichier dans un dossier,
    // Si c'est un fichier affiche juste que c'est un fichier,
    // Si il existe pas affiche "Dossier non existant"
    public static ArrayList<File> listDirectory(String path) {
        int dircount = 0;
        ArrayList<File> retour = new ArrayList();
        File file = new File(path);
        if (file.exists() && file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    if (files[i].isFile() == true) {
                        retour.add(files[i]);
                        dircount++;
                    }
                }
            }
            System.out.println("Le dossier existe");
            return retour;
        }
        else if ( file.exists() ){
            System.out.println("C'est un fihier");
            return null;
        } else {
            System.out.println("Dossier non existant");
            return null;
        }
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Choisir le dossier :");
        String str = sc.nextLine();

        ArrayList<File> files = listDirectory(str);
        for ( File fichier  : files ) {
            System.out.println("Fichier: " + fichier.getAbsolutePath());
        }
    }
}
