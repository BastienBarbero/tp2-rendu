import java.io.*;
import java.util.*;

public class Exercice4_Write {

    //la methode recupere le nom du fichier avec de l'extention
    //recupere le texte entrer par l'utilisateur et l'ecris ligne dans le fichier
    public static void main(String[] args) {

        FileWriter fos = null;
        Scanner scan = new Scanner(System.in), scan1 = new Scanner(System.in), sc = new Scanner(System.in);
        System.out.println("Entrer le nom du futur fichier (avec l'extension) :");
        String fichier = scan1.nextLine();
        System.out.println("Choisir le dossier du fichier :");
        String dossier = scan1.nextLine();
        String newline = System.getProperty("line.separator");

        try {
            System.out.println("ÉcriRE le texte à inscrire dans le fichier (saisir \"quit\" pour quitter) : ");
            StringBuffer contenuFichier = new StringBuffer();
            boolean continuer = true;

            // On boucle jusqu'à ce que l'user tape "quit"
            fos = new FileWriter(new File(dossier, fichier));
            while (continuer) {
                String tae = scan.nextLine();
                if (tae.toLowerCase().equals("quit")) continuer = false;
                else contenuFichier.append(tae + "\n");
                fos.write(tae);
                fos.write(newline);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fos != null)
                    fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}


