
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class Exercice2_ListWithExtend {


    // La methode recupere en entrer le path et l'extention
    // parcour le dossier et mets dans la liste les fichier qui ont l'extention donné en entrer
    //renvoi la liste des fichiers
    public static ArrayList<File> listWithExtend(String path, String extention) {

        int dircount = 0;
        ArrayList<File> retour = new ArrayList();
        File file = new File(path);
        if (file.exists() && file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    if (files[i].isFile() == true) {
                        String nomFichier = files[i].getName().toLowerCase();
                        if (nomFichier.endsWith(extention)) {
                            retour.add(files[i]);
                        }
                    }
                }
                return retour;
            }
        }
        return null;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Choisir le dossier :");
        String str = sc.nextLine();

        System.out.println("Choisir l'extention a filtrer ");
        String str2 = sc.nextLine();

        ArrayList<File> files = listWithExtend(str,str2);
        for ( File fichier  : files ) {
            System.out.println("Fichier: " + fichier.getAbsolutePath());
        }
    }
}


