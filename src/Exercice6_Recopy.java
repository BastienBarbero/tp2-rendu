import java.io.*;
import java.util.Scanner;


// Permet de choisir un fichier et de le recopie dans un autre fichier
public class Exercice6_Recopy {
    public static void main(String[] args) {

        FileInputStream fis = null;
        FileOutputStream fos = null;

        Scanner sc = new Scanner(System.in);
        System.out.println("Choisir le dossier du fichier a copier :");
        String strCopierP = sc.nextLine();

        System.out.println("Choisir le nom du fichier a copier :");
        String strCopierF = sc.nextLine();


        System.out.println("Choisir le dossier de la copie :");
        String strCopieP = sc.nextLine();

        System.out.println("Choisir le nom du fichier a copier :");
        String strCopieF = sc.nextLine();


        try {
            fis = new FileInputStream(new File(strCopierP,strCopierF));
            System.out.println("hey");
            fos = new FileOutputStream(new File(strCopieP,strCopieF));
            byte[] buf = new byte[8];
            int n = 0;
            while ((n = fis.read(buf)) >= 0) {
                fos.write(buf);
                for (byte bit : buf) {
                    System.out.print("\t" + bit + "(" + (char) bit + ")");
                }
                System.out.println("");
                buf = new byte[8];

            }
            System.out.println("Copie terminée !");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null)
                    fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (fos != null)
                    fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}