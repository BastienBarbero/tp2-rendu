import java.io.*;
import java.net.URL;
import java.util.Scanner;


//Télécharger une image et l'enrefistrer sur le disque dur
public class Exercice7_SaveImage {

    public static void saveImage(String imageUrl, String destinationFile, String nom) throws IOException {
        URL url = new URL(imageUrl);
        InputStream is = url.openStream();
        OutputStream os = new FileOutputStream(new File(destinationFile,nom));

        byte[] b = new byte[2048];
        int length;

        while ((length = is.read(b)) != -1) {
            os.write(b, 0, length);
        }

        is.close();
        os.close();
    }

    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);
        System.out.println("Choisir la destination d'enregistrement de l'image :");
        String str = sc.nextLine();

        System.out.println("Choisir le nom de l'enregistrement avec l'extention :");
        String str1 = sc.nextLine();

        System.out.println("Choisir l'URL de l'image :");
        String str2 = sc.nextLine();

        try {
            saveImage(str2,str,str1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}